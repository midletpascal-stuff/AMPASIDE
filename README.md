AMPASIDE
========

Advanced MIDletPascal IDE

Среда разработки использующая компилятор языка программирования [MIDletPascal](http://ru.wikipedia.org/wiki/MIDletPascal).

Оригинальная версия разработана Helltar и размещена тут: https://github.com/Helltar/AMPASIDE. Я занимаюсь поддержкой проекта

Установка
---------

- [install_linux.md](https://github.com/Helltar/AMPASIDE/blob/master/install_linux.md)
- [install_windows.md](https://github.com/Helltar/AMPASIDE/blob/master/install_windows.md)

Сборка из исходников
--------------------

[![LazarusIDE](http://wiki.lazarus.freepascal.org/images/9/94/built_with_lazarus_logo.png)](http://www.lazarus-ide.org)

Лицензия
--------

[![GPLv3](http://www.gnu.org/graphics/gplv3-127x51.png)](https://github.com/Helltar/AMPASIDE/blob/master/COPYING)

Скриншоты
---------

##### Linux (KDE)

![KDE](https://sun9-19.userapi.com/c855620/v855620502/137782/2ukYiJIbu0Y.jpg)

##### Windows

![Windows](https://sun9-18.userapi.com/c855620/v855620502/13778b/8wkDJ40cbo8.jpg)
